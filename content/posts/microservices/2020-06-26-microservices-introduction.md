---
title: Microservices là gì?
date: 2020-06-26
tags: ["microservices"]
---

Đây là bài viết đầu tiên trong 7 phần của series bài viết về kiến trúc Microservices.
Series này sẽ tập trung định nghĩa các thành phần của kiến trúc microservice. Cung
cấp cho các bạn về ưu nhược điểm của kiến trúc Microservices và làm thế nào để áp
dụng vào project của bạn.

Trong bài viết này, chúng ta sẽ đề cập đến cách thiết kế, build và deploy hệ thống
microservices, hướng tiếp cận và sự khác biệt so với các hệ thống 
[Monolithic](https://microservices.io/patterns/monolithic.html) truyền thống. 

#### Kiến trúc Monolithic
![Monolithic](https://quangdangfit.gitlab.io/blog/static/img/microservices-part1-1_monolithic-architecture.png)